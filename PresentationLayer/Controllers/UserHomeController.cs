﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using PresentationLayer.Models;
using System.Net;
using DAL.Entities.Enumerations;
using PresentationLayer.Models.ViewModels;
using PresentationLayer.Models.ViewModels.VmBuilders.Interfaces;

namespace PresentationLayer.Controllers
{
    [Authorize(Roles = "user")]
    public class UserHomeController : Controller
    {
        private readonly IHotelsManager _hotelsManager;
        private readonly IHotelsVmBuilder _hotelsVmBuilder;

        public UserHomeController(IHotelsManager managersProvider, IHotelsVmBuilder hotelsVmBuilder)
        {
            _hotelsManager = managersProvider;
            _hotelsVmBuilder = hotelsVmBuilder;
        }

        public ActionResult Index()
        {
            IEnumerable<HotelDTO> hotelsDtos = _hotelsManager.GetAll().Take(5);
            Mapper.Initialize(cfg => cfg.CreateMap<HotelDTO, HotelViewModel>());
            var hotels = Mapper.Map<IEnumerable<HotelDTO>, List<HotelViewModel>>(hotelsDtos);

            return View(_hotelsVmBuilder.GeHotelsListViewModel(hotels, new HotelViewModel(), ""));
        }

        [HttpPost]
        public ActionResult Index([Bind(Prefix = "Hotel")]HotelViewModel hotel)
        {
            return View("Create", hotel);
        }



        [HttpPost]
        public ActionResult GetHotels(string Name)
        {
            IEnumerable<HotelDTO> hotelsDtos = _hotelsManager.GetHotelsByName(Name);

            Mapper.Initialize(cfg => cfg.CreateMap<HotelDTO, HotelViewModel>());
            var hotels = Mapper.Map<IEnumerable<HotelDTO>, List<HotelViewModel>>(hotelsDtos).Take(5);

            return PartialView("HotelsList", hotels);
        }

        public ActionResult Create()
        {
            return View();
        }

        // POST: /Default1/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HotelViewModel hotel)
        {
            if (ModelState.IsValid)
            {
                Mapper.Initialize(cfg => cfg.CreateMap<HotelViewModel, HotelDTO>());

                var hotelDto = Mapper.Map<HotelViewModel, HotelDTO>(hotel);

                _hotelsManager.AddHotel(hotelDto.Name, hotelDto.Location, hotelDto.Category, null);

                return RedirectToAction("Smile");
            }

            return View(hotel);
        }

        public ActionResult Smile()
        {
            return View();
        }
    }
}