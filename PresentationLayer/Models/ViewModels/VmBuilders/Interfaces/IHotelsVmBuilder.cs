﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentationLayer.Models.ViewModels.VmBuilders.Interfaces
{
    public interface IHotelsVmBuilder
    {
        HotelsListViewModel GeHotelsListViewModel(IEnumerable<HotelViewModel> hotelList, HotelViewModel hotel, string name);
    }
}
