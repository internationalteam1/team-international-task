﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PresentationLayer.Models.ViewModels.VmBuilders.Interfaces;

namespace PresentationLayer.Models.ViewModels.VmBuilders.Concrete
{
    public class HotelsVmBuilder : IHotelsVmBuilder
    {
        public HotelsListViewModel GeHotelsListViewModel(IEnumerable<HotelViewModel> hotelsList, HotelViewModel hotel, string name)
        {
            return new HotelsListViewModel()
            {
                Name = name,
                HotelsList = hotelsList,
                Hotel = hotel
            };
        }
    }
}