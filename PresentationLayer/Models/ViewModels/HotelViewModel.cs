﻿using System.Collections.Generic;
using DAL.Entities.Enumerations;
using Ninject.Activation;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer.Models
{
    public class HotelViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Ввведите название")]
        public string Name { get; set; }

        [Display(Name = "Расположение")]
        [Required(ErrorMessage = "Введите расположение")]
        public string Location { get; set; }

        [Display(Name = "Категория")]
        [Required(ErrorMessage = "Выберите категорию")]
        public  CategoryHotel Category { get; set; }
    }
}
