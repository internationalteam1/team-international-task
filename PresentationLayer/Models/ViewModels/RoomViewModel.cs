﻿
namespace PresentationLayer.Models
{
    public class RoomViewModel
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public int Number { get; set; }
        public int Category { get; set; }
        public decimal Price { get; set; }
        public int Couchettes { get; set; }
    }
}
