﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PresentationLayer.Models.ViewModels
{
    public class HotelsListViewModel
    {
        public IEnumerable<HotelViewModel> HotelsList { get; set; }

        public HotelViewModel Hotel { get; set; }

        [Display(Name = "Название отеля")]
        public string Name { get; set; }
    }
}