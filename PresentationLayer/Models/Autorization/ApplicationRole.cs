﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace PresentationLayer.Models
{
    public class ApplicationRole : IdentityRole
    {
        public string Description { get; set; }
    }

    public class CreateRoleModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}

//Использование моделей позволит избежать различных проблем с контекстом данных и управлением объектами, которые могли возникнуть, 
//если бы мы напрямую использовали бы ApplicationRole.