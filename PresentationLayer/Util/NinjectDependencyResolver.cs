﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BLL.Interfaces;
using BLL.Managers;
using Ninject;
using PresentationLayer.Models.ViewModels.VmBuilders.Concrete;
using PresentationLayer.Models.ViewModels.VmBuilders.Interfaces;

namespace PresentationLayer.Util
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
        private void AddBindings()
        {
       ////     kernel.Bind<IClientsManager>().To<ClientsDbManager>();
            kernel.Bind<IHotelsManager>().To<HotelsDbManager>();
            kernel.Bind<IHotelsVmBuilder>().To<HotelsVmBuilder>();
            //    kernel.Bind<IOrdersManager>().To<OrdersDbManager>();
            //    kernel.Bind<IRoomsManager>().To<RoomsDbManager>();
            //    kernel.Bind<IServicesManager>().To<ServicesDbManager>();
        }
    }
}