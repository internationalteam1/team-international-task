﻿using DAL.Entities;
using System.Data.Entity;

namespace DAL.EF
{
    public class HotelsContext:DbContext
    {
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Service> Services { get; set; }
        public HotelsContext(string connectionString)
        : base(connectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Room>().HasMany(p => p.Orders).WithMany(k => k.Rooms);
            modelBuilder.Entity<Room>().HasRequired(p => p.Hotel).WithMany(k => k.Rooms);
            modelBuilder.Entity<Room>().HasMany(p => p.Services).WithMany(k => k.Rooms);

            modelBuilder.Entity<Order>().HasMany(p => p.Services).WithMany(k => k.Orders);
            modelBuilder.Entity<Order>().HasRequired(k => k.Client);

            base.OnModelCreating(modelBuilder);

        }
    }
}
