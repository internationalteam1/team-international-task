﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private HotelsContext db;
        public ClientRepository(string connectionString)
        {
           db = new HotelsContext(connectionString);
        }

        public Client Create(Client item)
        {
            return db.Clients.Add(item);
        }

        public void Delete(int id)
        {
            var client = db.Clients.Find(id);
            if (client != null)
                db.Clients.Remove(client);

        }

        public IEnumerable<Client> Find(Func<Client, bool> predicate)
        {
            return db.Clients.Where(predicate).ToList();
        }

        public Client Get(int id)
        {
            return db.Clients.Find(id);
        }

        public IEnumerable<Client> GetAll()
        {
            return db.Clients.ToList();
        }

        public void Update(Client item)
        {
            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            db.SaveChanges();
        }
    }
}
