﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class ServiceRepository:IServiceRepository
    {
        private HotelsContext db;
        public ServiceRepository(string connectionString)
        {
            db = new HotelsContext(connectionString);
        }

        public Service Create(Service item)
        {
            return db.Services.Add(item);
        }

        public void Delete(int id)
        {
            var service = db.Services.Find(id);
            if (service != null)
                db.Services.Remove(service);
        }

        public IEnumerable<Service> Find(Func<Service, bool> predicate)
        {
            return db.Services.Where(predicate).ToList();
        }

        public Service Get(int id)
        {
            return db.Services.Find(id);
        }

        public IEnumerable<Service> GetAll()
        {
            return db.Services.ToList();
        }

        public void Update(Service item)
        {
            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            db.SaveChanges();
        }
    }
}
