﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class RoomRepository : IRoomRepository
    {
        private HotelsContext db;
        public RoomRepository(string connectionString)
        {
            db = new HotelsContext(connectionString);
        }
        public Room Get(int id)
        {
            return db.Rooms.Find(id);
        }
        public IEnumerable<Room> GetAll()
        {
            return db.Rooms.ToList();
        }
        public Room Create(Room item)
        {
            return db.Rooms.Add(item);
        }
        public void Delete(int id)
        {
            Room room = db.Rooms.Find(id);
            if (room != null)
                db.Rooms.Remove(room);
        }
        public IEnumerable<Room> Find(Func<Room, bool> predicate)
        {
            return db.Rooms.Where(predicate).ToList();
        }

        public void Update(Room item)
        {
            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            db.SaveChanges();
        }
    }
}
