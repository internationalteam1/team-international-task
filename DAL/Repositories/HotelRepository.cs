﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class HotelRepository:IHotelRepository
    {
        private HotelsContext db;
        public HotelRepository(string connectionString)
        {
            db = new HotelsContext(connectionString);
        }
        public Hotel Create(Hotel item)
        {
            return db.Hotels.Add(item);
        }

        public void Delete(int id)
        {
            var hotel = db.Hotels.Find(id);
            if (hotel != null)
                db.Hotels.Remove(hotel);
        }

        public IEnumerable<Hotel> Find(Func<Hotel, bool> predicate)
        {
            return db.Hotels.Where(predicate).ToList();
        }

        public Hotel Get(int id)
        {
            return db.Hotels.Find(id);
        }

        public IEnumerable<Hotel> GetAll()
        {
            return db.Hotels.ToList();
        }

        public void Update(Hotel item)
        {
            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            db.SaveChanges();
        }
    }
}
