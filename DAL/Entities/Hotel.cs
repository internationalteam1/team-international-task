﻿using DAL.Entities.Enumerations;
using System.Collections.Generic;


namespace DAL.Entities
{
    public class Hotel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public CategoryHotel Category { get; set; }

        public virtual ICollection<Room> Rooms { get; set; }
        public Hotel()
        {
            Rooms = new List<Room>();
        }

    }
}
