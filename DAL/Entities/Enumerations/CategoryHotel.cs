﻿namespace DAL.Entities.Enumerations
{
    public enum CategoryHotel
    {
        OneStar =1,
        TwoStar,
        ThreeStar,
        FourStar,
        FiveStar
    }
}
