﻿namespace DAL.Entities.Enumerations
{
    public enum BookingState
    {
        Paid=1,
        NotPaid
    }
}
