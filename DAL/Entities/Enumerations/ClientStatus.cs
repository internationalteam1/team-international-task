﻿namespace DAL.Entities.Enumerations
{
    public enum ClientStatus
    {
        New,
        Bronze,
        Silver,
        Gold,
        VIP
    }
}
