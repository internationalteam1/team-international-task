﻿namespace DAL.Entities.Enumerations
{
    public enum CategoryRoom
    {
        Standart = 1,
        HalfLux,
        Lux
    }
}
