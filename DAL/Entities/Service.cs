﻿using System.Collections.Generic;


namespace DAL.Entities
{
    public class Service
    {
        public int Id { get; set; }
        public string  Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Room> Rooms { get; set; }

        public Service()
        {
            Orders = new List<Order>();
            Rooms = new List<Room>();
        }
    }
}
