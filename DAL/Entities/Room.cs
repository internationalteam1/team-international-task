﻿using DAL.Entities.Enumerations;
using System.Collections.Generic;


namespace DAL.Entities
{
    public class Room
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public int Number { get; set; }
        public CategoryRoom Category { get; set; }
        public decimal Price { get; set; }
        public int Couchettes { get; set; }

        public virtual Hotel Hotel { get; set; }
        public virtual ICollection<Service> Services { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

        public Room()
        {
            Orders = new List<Order>();
            Services = new List<Service>();
        }
    }
}
