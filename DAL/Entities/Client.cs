﻿using DAL.Entities.Enumerations;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class Client
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public ClientStatus Status { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public Client()
        {
            Orders = new List<Order>();
        }
    }
}
