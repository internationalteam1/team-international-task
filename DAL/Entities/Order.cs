﻿using DAL.Entities.Enumerations;
using System;
using System.Collections.Generic;


namespace DAL.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public decimal TotalCost { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public BookingState BookingState { get; set; }
        public OrderState OrderState { get; set; }

        public Client Client { get; set; }
        public virtual ICollection<Room> Rooms { get; set; }
        public virtual ICollection<Service> Services { get; set; }

        public Order()
        {
            Services = new List<Service>();
            Rooms = new List<Room>();
        }
    }
}
