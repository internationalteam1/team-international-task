﻿using System;

namespace BLL.DTO
{
    public class OrderDTO
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public decimal TotalCost { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int BookingState { get; set; }
        public int OrderState { get; set; }
    }
}
