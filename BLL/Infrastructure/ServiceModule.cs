﻿using DAL.Interfaces;
using DAL.Repositories;
using Ninject.Modules;

namespace BLL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private readonly string _connectionString;
        public ServiceModule(string connection)
        {
            _connectionString = connection;
        }
        public override void Load()
        {
            Bind<IClientRepository>().To<ClientRepository>().WithConstructorArgument(_connectionString);
            Bind<IHotelRepository>().To<HotelRepository>().WithConstructorArgument(_connectionString);
            Bind<IOrderRepository>().To<OrderRepository>().WithConstructorArgument(_connectionString);
            Bind<IRoomRepository>().To<RoomRepository>().WithConstructorArgument(_connectionString);
            Bind<IServiceRepository>().To<ServiceRepository>().WithConstructorArgument(_connectionString);          
        }
    }
}
