﻿using DAL.Interfaces;

namespace BLL.Interfaces
{
    public interface IRoomsManager
    {
        IRoomRepository Repository { get; }
    }
}
