﻿using System;
using System.Collections.Generic;
using BLL.DTO;
using DAL.Interfaces;

namespace BLL.Interfaces
{
    public interface IOrdersManager
    {
        IEnumerable<OrderDTO> GetOrdersByHotelId(int hotelId);
        IEnumerable<OrderDTO> GetOrdersByClientId(int clientId);
        IEnumerable<OrderDTO> GetOrdersByRoomId(int roomId);
        bool TryAddOrder(DateTime dateFrom, DateTime dateTo,int bookingState, int orderState, ClientDTO client,
            IEnumerable<int> rooms, IEnumerable<int> services);

        IOrderRepository Repository { get; }
    }
}
