﻿using DAL.Interfaces;

namespace BLL.Interfaces
{
    public interface IServicesManager
    {
        IServiceRepository Repository { get; }
    }
}
