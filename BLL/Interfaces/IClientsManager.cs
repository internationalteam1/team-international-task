﻿using System.Collections.Generic;
using DAL.Interfaces;
using BLL.DTO;

namespace BLL.Interfaces
{
    public interface IClientsManager
    {
        IEnumerable<ClientDTO> GetAllClientsByHotelId(int hotelId);
        
        //Клиенты с открытыми заказами
        IEnumerable<ClientDTO> GetActiveClientsByHotelId(int hotelId);

        //Клиенты с оплаченными заказами
        IEnumerable<ClientDTO> GetClientsWithPaymentByHotelId(int hotelId);

        bool UpdateClientStatus(int clientId);

        //Скидка в процентах
        double GetDiscountByClientStatus(int clientStatus);

        IClientRepository Repository { get; }
    }
}
