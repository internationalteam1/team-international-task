﻿using System.Collections.Generic;
using BLL.DTO;
using DAL.Entities;
using DAL.Entities.Enumerations;
using DAL.Interfaces;

namespace BLL.Interfaces
{
    public interface IHotelsManager
    {
        IEnumerable<HotelDTO> GetHotelsByName(string name);
        IEnumerable<HotelDTO> GetHotelsByLocation(string location);
        IEnumerable<HotelDTO> GetHotelsByCategory(int category);
        IEnumerable<HotelDTO> GetAll();
        HotelDTO AddHotel(string name, string location, int category, IEnumerable<int> rooms);
        HotelDTO UpdateHotelById(int hotelId, string name, string location, int category, IEnumerable<int> rooms);
        RoomDTO AddRoom(int hotelId, int number, int category, decimal price, int couchettes, IEnumerable<int> services);

        IHotelRepository Repository { get; }
    }
}
