﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Entities.Enumerations;
using DAL.Interfaces;

namespace BLL.Managers
{
    public class HotelsDbManager : IHotelsManager
    {
        private readonly IHotelRepository _hotelRepository;
        private readonly IRoomRepository _roomRepository;
        private readonly IServiceRepository _serviceRepository;

        private readonly IMapper _mapper;

        public HotelsDbManager(IHotelRepository hotelRepository, IRoomRepository roomRepository, IServiceRepository serviceRepository)
        {
            this._hotelRepository = hotelRepository;
            this._roomRepository = roomRepository;
            this._serviceRepository = serviceRepository;

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Hotel, HotelDTO>();
                cfg.CreateMap<Room, RoomDTO>();
            });

            this._mapper = config.CreateMapper();
        }

        public IHotelRepository Repository
        {
            get { return _hotelRepository; }
        }

        public IEnumerable<HotelDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<Hotel>, List<HotelDTO>>(_hotelRepository.GetAll());
        }

        public HotelDTO AddHotel(string name, string location, int category, IEnumerable<int> rooms = null)
        {
            CategoryHotel hc;
            Enum.TryParse(category.ToString(), out hc);

            Hotel hotel = new Hotel()
            {
                Name = name,
                Location = location,
                Category = hc
            };

            if (rooms != null)
                hotel.Rooms = _roomRepository.Find(r => rooms.Contains(r.Id)).ToList();

            Hotel createdHotel = _hotelRepository.Create(hotel);

            _hotelRepository.Save();

            return _mapper.Map<Hotel, HotelDTO>(createdHotel);
        }

        public IEnumerable<HotelDTO> GetHotelsByCategory(int category)
        {
            CategoryHotel hc;
            Enum.TryParse(category.ToString(), out hc);

            return _mapper.Map<IEnumerable<Hotel>, List<HotelDTO>>(_hotelRepository.GetAll().Where(h => h.Category == hc));
        }

        public IEnumerable<HotelDTO> GetHotelsByLocation(string location)
        {
            return _mapper.Map<IEnumerable<Hotel>, List<HotelDTO>>(_hotelRepository.Find(h => h.Location.Contains(location)));
        }

        public IEnumerable<HotelDTO> GetHotelsByName(string name)
        {
            return _mapper.Map<IEnumerable<Hotel>, List<HotelDTO>>(_hotelRepository.Find(h => h.Name.Contains(name)));
        }

        public HotelDTO UpdateHotelById(int hotelId, string name, string location, int category, IEnumerable<int> rooms = null)
        {
            Hotel hotel = _hotelRepository.Get(hotelId);

            if (hotel == null)
                return null;

            CategoryHotel hc;
            Enum.TryParse(category.ToString(), out hc);

            hotel.Name = name;
            hotel.Location = location;
            hotel.Category = hc;
            if (rooms != null)
                hotel.Rooms = _roomRepository.Find(r => rooms.Contains(r.Id)).ToList();

            _hotelRepository.Update(hotel);

            return _mapper.Map<Hotel, HotelDTO>(hotel);
        }

        public RoomDTO AddRoom(int hotelId, int number, int category, decimal price, int couchettes, IEnumerable<int> services)
        {
            Hotel hotel = _hotelRepository.Get(hotelId);

            if (hotel == null)
                return null;

            CategoryRoom rc;
            Enum.TryParse(category.ToString(), out rc);

            var room = new Room()
            {
                Number = number,
                Category = rc,
                Price = price,
                Couchettes = couchettes,
                Hotel = hotel,
            };

            if (services != null)
                room.Services = _serviceRepository.Find(s => services.Contains(s.Id)).ToList();

            Room createdRoom = _roomRepository.Create(room);
            _roomRepository.Save();

            hotel.Rooms.Add(createdRoom);

            _hotelRepository.Save();

            return _mapper.Map<Room, RoomDTO>(room);
        }
    }
}
