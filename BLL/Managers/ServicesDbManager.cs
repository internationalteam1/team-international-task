﻿using BLL.Interfaces;
using DAL.Interfaces;

namespace BLL.Managers
{
    public class ServicesDbManager : IServicesManager
    {
        private readonly IServiceRepository _serviseRepository;

        public ServicesDbManager(IServiceRepository serviseRepository)
        {
            _serviseRepository = serviseRepository;
        }
        public IServiceRepository Repository
        {
            get { return _serviseRepository; }
        }
    }
}
