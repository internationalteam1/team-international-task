﻿using BLL.Interfaces;
using DAL.Interfaces;

namespace BLL.Managers
{
    public class RoomsDbManager : IRoomsManager
    {
        private readonly IRoomRepository _roomRepository;

        public RoomsDbManager(IRoomRepository roomRepository)
        {
            _roomRepository = roomRepository;
        }

        public IRoomRepository Repository
        {
            get { return _roomRepository; }
        }
    }
}
