﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Entities.Enumerations;
using DAL.Interfaces;

namespace BLL.Managers
{
    public class OrdersDbManager : IOrdersManager
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IHotelRepository _hotelRepository;
        private readonly IRoomRepository _roomRepository;
        private readonly IClientRepository _clientRepository;
        private readonly IServiceRepository _serviceRepository;

        private readonly IMapper _mapper;

        public OrdersDbManager(IOrderRepository orderRepository, IHotelRepository hotelRepository, IRoomRepository roomRepository, IClientRepository clientRepository,IServiceRepository serviceRepository)
        {
           _orderRepository = orderRepository;
           _hotelRepository = hotelRepository;
            _roomRepository = roomRepository;
            _clientRepository = clientRepository;
            _serviceRepository = serviceRepository;

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Order, OrderDTO>();
            });

            this._mapper = config.CreateMapper();
        }

        public IEnumerable<OrderDTO> GetOrdersByClientId(int clientId)
        {
            // применяем автомаппер для проекции одной коллекции на другую
            return Mapper.Map<IEnumerable<Order>, List<OrderDTO>>(_orderRepository.Find(p=>p.ClientId==clientId));
        }

        public IEnumerable<OrderDTO> GetOrdersByHotelId(int hotelId)
        {
            Hotel hotel = _hotelRepository.Get(hotelId);
            if (hotel == null)
                return null;

            return Mapper.Map<IEnumerable<Order>, List<OrderDTO>>(_orderRepository
                .Find(o => o.Rooms.First().HotelId == hotelId));
        }

        public IEnumerable<OrderDTO> GetOrdersByRoomId(int roomId)
        {
            return Mapper.Map<IEnumerable<Order>, List<OrderDTO>>(_orderRepository.Find(o => o.Rooms.Any(r => r.Id == roomId)));
        }

        public bool TryAddOrder(DateTime dateFrom, DateTime dateTo, int bookingState, int orderState, ClientDTO clientDto,
            IEnumerable<int> rooms, IEnumerable<int> services = null)
        {
            if (rooms == null || !rooms.Any())
                return false;

            foreach (var room in rooms)
            {
                bool isNotEnable = _orderRepository //Занята ли комната в течение периода с dateFrom до dateTo
                     .Find(o => o.Rooms.Any(r => r.Id == _roomRepository.Get(room).Id) &&
                     (IsBetween(dateFrom, o.DateFrom, o.DateTo) || IsBetween(dateTo, o.DateFrom, o.DateTo) ||
                     IsBetween(o.DateFrom, dateFrom, dateTo) || IsBetween(o.DateTo, dateFrom, dateTo)))
                     .Any();

                if (isNotEnable)
                    return false;
            }
            BookingState bs;
            Enum.TryParse(bookingState.ToString(), out bs);
            OrderState os;
            Enum.TryParse(orderState.ToString(), out os);
            Order order = new Order()
            {
                DateFrom = dateFrom,
                DateTo = dateTo,
                BookingState = bs,
                OrderState = os,
                Client =_clientRepository.Get(clientDto.Id),
                Rooms =_roomRepository.Find(p=> rooms.Contains(p.Id)).ToList(),
                TotalCost = _roomRepository.Find(p => rooms.Contains(p.Id)).Sum(r => r.Price)
            };
            if (services != null)
                order.Services = _serviceRepository.Find(p => services.Contains(p.Id)).ToList();

            _orderRepository.Create(order);
            _orderRepository.Save();

            return true;
        }

        private bool IsBetween(DateTime target, DateTime start, DateTime end)
        {
            return target > start && target < end;
        }

        public IOrderRepository Repository
        {
            get { return _orderRepository; }
        }
    }
}
