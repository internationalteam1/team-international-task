﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Entities.Enumerations;
using DAL.Interfaces;

namespace BLL.Managers
{
    public class ClientsDbManager : IClientsManager
    {
        private readonly IClientRepository _clientRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrdersManager _ordersManager;

        private readonly IMapper _mapper;

        public ClientsDbManager(IClientRepository clientRepository,
            IOrderRepository orderRepository,
            IOrdersManager ordersManager)
        {
            this._clientRepository = clientRepository;
            this._orderRepository = orderRepository;
            this._ordersManager = ordersManager;

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Client, ClientDTO>();
            });

            this._mapper = config.CreateMapper();
        }

        public IClientRepository Repository
        {
            get { return _clientRepository; }
        }
        public IEnumerable<ClientDTO> GetAllClientsByHotelId(int hotelId)
        {
            return _mapper.Map<IEnumerable<Client>, List<ClientDTO>>(_ordersManager.GetOrdersByHotelId(hotelId)
                .Select(o => _clientRepository.Get(o.ClientId)));
        }

        public IEnumerable<ClientDTO> GetActiveClientsByHotelId(int hotelId)
        {
            return _mapper.Map<IEnumerable<Client>, List<ClientDTO>>(_ordersManager.GetOrdersByHotelId(hotelId)
                .Where(o => o.OrderState == ((int)OrderState.Closed))
                .Select(o => _clientRepository.Get(o.ClientId)));
        }

        public IEnumerable<ClientDTO> GetClientsWithPaymentByHotelId(int hotelId)
        {
            return _mapper.Map<IEnumerable<Client>, List<ClientDTO>>(_ordersManager.GetOrdersByHotelId(hotelId)
                 .Where(o => o.BookingState == (int)BookingState.Paid)
                 .Select(o => _clientRepository.Get(o.Id)));
        }

        public double GetDiscountByClientStatus(int clientStatus)
        {
            double discount = 0;
            switch (clientStatus)
            {
                case (int)ClientStatus.New:
                    break;
                case (int)ClientStatus.Bronze:
                    discount = 2;
                    break;
                case (int)ClientStatus.Silver:
                    discount = 5;
                    break;
                case (int)ClientStatus.Gold:
                    discount = 10;
                    break;
                case (int)ClientStatus.VIP:
                    discount = 20;
                    break;
            }
            return discount;
        }

        //Учитывается только суммарная стоимость заказов
        public bool UpdateClientStatus(int clientId)
        {
            Client client = _clientRepository.Get(clientId);

            if (client == null)
                return false;

            decimal ordersTotalCost = _orderRepository
                .Find(o => o.Client.Id == clientId && o.BookingState == BookingState.Paid)
                .Sum(o => o.TotalCost);

            if (Between(ordersTotalCost, 1000, 3000))
                client.Status = ClientStatus.Bronze;

            if (Between(ordersTotalCost, 5000, 7000))
                client.Status = ClientStatus.Silver;

            if (Between(ordersTotalCost, 7000, 10000))
                client.Status = ClientStatus.Gold;

            if (ordersTotalCost > 15000)
                client.Status = ClientStatus.VIP;

            _clientRepository.Update(client);
            _clientRepository.Save();

            return true;
        }

        private bool Between(decimal value, decimal start, decimal end)
        {
            return value > start && value < end;
        }
    }
}
