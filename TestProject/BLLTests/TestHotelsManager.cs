﻿/*using System.Linq;
using BLL.Interfaces;
<<<<<<< HEAD
using BLL.Managers.Concrete;
using BLL.Managers.Interfaces;
=======
using BLL.Managers;
>>>>>>> b4b7f8c15176d9ea14812ee1092d8000c296fb2e
using DAL.EF;
using DAL.Entities;
using DAL.Entities.Enumerations;
using DAL.Repositories;
using Unit = NUnit.Framework;
using NUnit.Framework;
    */
namespace TestProject.BLLTests
{
    /*
    [TestFixture]
    public class TestHotelsManager
    {
        private HotelsContext hotelsContext = new HotelsContext();
        private IHotelsManager hotelsManager;
        public TestHotelsManager()
        {
            hotelsManager = new HotelsDbManager(new HotelRepository(hotelsContext), new RoomRepository(hotelsContext));
        }

        [Test]
        public void TestAddingHotels()
        {
            hotelsContext.Database.Delete();
            hotelsContext.Database.Create();

            //Arrange
            Hotel hotel = new Hotel()
            {
                Category = CategoryHotel.OneStar,
                Name = "TestHotel",
                Location = "London",
                Rooms = null
            };

            Room room = new Room()
            {
                Category = CategoryRoom.Lux,
                Couchettes = 2,
                Hotel = hotel,
                Number = 111,
                Price = 100,
                Services = null,
                Orders = null
            };

            //Act
            Hotel createdHotel = hotelsManager.AddHotel(hotel.Name, hotel.Location, hotel.Category, null);

            hotelsManager.AddRoom(createdHotel.Id, room.Number, room.Category, room.Price, room.Couchettes, room.Services);

            //Assert
            Unit.Assert.AreEqual(true, hotelsContext.Hotels.Any());
            Unit.Assert.AreEqual(true, hotelsManager.GetHotelsByName("Test").Any());
            Unit.Assert.AreEqual(true, hotelsContext.Rooms.Any());
            Unit.Assert.AreEqual(true, hotelsManager.GetHotelsByName("Test").First().Rooms.Any());
        }
    }
    */
}
